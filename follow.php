<?php
	session_start();
	include "connect.php";
	include "func.php";

	$user_id = $_SESSION['id'];


 // AJAX REQUEST FÖR ATT FÖLJA OCH AVFÖLJA ANVÄNDARE
	if (isset($_GET['follow'])) {
		$follow = mysqli_real_escape_string($link,$_GET['follow']);
		
		mysqli_query($link, "INSERT INTO `followers` (`user_id`,`following`) VALUES ($user_id,'$follow')");
		mysqli_error($link); 
	}

	if (isset($_GET['unfollow'])) {
		$unfollow = mysqli_real_escape_string($link,$_GET['unfollow']);

		mysqli_query($link,"DELETE FROM followers WHERE `user_id` = $user_id AND `following` = '$unfollow'");
		mysqli_error($link);
	}


// AJAX REQUEST FÖR ATT HÄMTA SÖKRESULTAT

if (isset($_GET['search'])) {
	$search         = mysqli_real_escape_string($link,$_GET['search']);
	$searchResult   = mysqli_query($link, 
		"SELECT `username`,`firstname`, `lastname` 
		 FROM user 
		 WHERE `username` LIKE '%$search%' OR
		 `firstname` LIKE '%$search%' OR
		 `lastname` LIKE '%$search%'
		 LIMIT 5");
	
	while ($s = mysqli_fetch_assoc($searchResult)) {
		print createlinks($s['username']) . "<br>";
	}
}

// Responsiva sökresultatet query 
if (isset($_GET['search2'])) {
	$search2         = mysqli_real_escape_string($link,$_GET['search2']);
	$searchResult2   = mysqli_query($link, 
		"SELECT `username`,`firstname`, `lastname` 
		 FROM user 
		 WHERE `username` LIKE '%$search2%' OR
		 `firstname` LIKE '%$search2%' OR
		 `lastname` LIKE '%$search2%'
		 LIMIT 5");
	
	while ($se = mysqli_fetch_assoc($searchResult2)) {
		print createlinks($se['username']) . "<br>";
	}
}

// TAR BORT TWEET
	if (isset($_GET['delete'])) {
		$tweet_id = (int)$_GET['delete'];
		mysqli_query($link,"DELETE FROM tweets WHERE `id` = $tweet_id");
		mysqli_error($link);
	};


// lägger till konversationsmeddelande i databas
	if (isset($_POST['message']) && isset($_POST['tweetId'])) {
 
		$convoId       = (int)$_POST['tweetId'];
		$convoMessage  = mysqli_real_escape_string($link,strip_tags($_POST['message'],"<a>"));
		$username      = mysqli_real_escape_string($link,$_SESSION['username']);
		$match = "";

		mysqli_query($link,"INSERT INTO conversations (`message`,`tweet_id`, `username`) 
		VALUES ('$convoMessage', $convoId, '$username')");

		// lägger till hashtag i databas
		addHashtag($link, $username, $convoId, $_POST['message']);

		// skapar notification om det behövs
		notify($link, $username, $convoId, $convoMessage);
	
		// skickar tillbaka json
		$arr = array("username" => createLinks($_SESSION['username']), "message" => hashtagsAndUser($convoMessage));
		print json_encode($arr);

};


?> 