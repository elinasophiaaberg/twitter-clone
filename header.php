<?php
session_start();

if (!isset($_SESSION['username'])) {
	header("Location: login.php");
}

if (isset($_GET['user'])) {

	if (strtolower($_GET['user']) == strtolower($_SESSION['username'])){
		header("Location: profile.php");
    }
}	

include_once("inc/connect.php");
include_once("inc/func.php");
require_once("inc/dbQueries.php");

$username = $_SESSION['username'];



?>


<!DOCTYPE html>
<html lang="sv">
<head>
	<meta charset="UTF-8">
	<title>LOL</title>
	<link rel="stylesheet" href="css/style.css"> 
	<link rel="stylesheet" href="css/ionicons.css"> 
	<link rel="stylesheet" href="css/animate.css"> 
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<script type="text/javascript" src="js/jquery.js"></script>
	<script src="//cdnjs.cloudflare.com/ajax/libs/jquery-form-validator/2.1.47/jquery.form-validator.min.js"></script>



</head>
<body>

<!-- lightbox (display:none)-->
<div class='bigbox'><div class='lightbox'></div></div>

<!-- sidopanelen -->
<?php include_once("left-sidebar.php"); ?> 
<?php include_once("right-sidebar.php"); ?> 


<!--  navigationsmenyn -->
<div class="navmenubottom"> 
	<div class="navtoggle">
 		<?php include_once("mobilenav.php"); ?> 
 	</div>
</div>

<div id="wrapper">
	
	<div class="navigation-menu">
		<a href="index.php">   Home    </a>
		<a href="profile.php"> profile </a>
		<a href="logout.php">  Logout  </a>
	</div>

	<!-- Logotypen  -->
	<center><a href="index.php"><img class="logo" src="images/logo.png" width="107" alt=""></a></center>