<?php
	

	if (isset($_GET['user'])) {
		if ($_GET['user'] == $_SESSION['username']){
			header("Location: profile.php");
	    }
	}	

	$tweetError = "";
	$imageError = "";

// Hämtar data från den som är inloggad
	$user_id = $_SESSION['id'];
	$loggedResult = mysqli_query($link,"SELECT firstname, lastname, username, email FROM user WHERE id = $user_id");

	while($u = mysqli_fetch_assoc($loggedResult)) {
		
		$loggedIn[] = $u;
	}

	$username = mysqli_real_escape_string($link,$loggedIn[0]["username"]);
	$firstname = mysqli_real_escape_string($link,$loggedIn[0]["firstname"]);
	$lastname = mysqli_real_escape_string($link,$loggedIn[0]["lastname"]);
	$email = mysqli_real_escape_string($link,$loggedIn[0]["email"]);




// Lägger till en ny tweet till databasen
if (isset($_POST['submitTweet'])) {

	$tweet    = mysqli_real_escape_string($link, $_POST['tweet']);

	$tweetImg = $_FILES['tweetImage']['tmp_name'];
	$imgError = $_FILES['tweetImage']['error'];
	$imgSize  = $_FILES['tweetImage']['size'];

	$insertTweetQuery = "INSERT INTO tweets (`user_id`, `username`, `tweet`, `date`) 
						VALUES ($user_id, '$username', '$tweet', CURRENT_TIMESTAMP)";

	// om det finns en bild					
	if (file_exists($tweetImg)) {
		$uploadedImage = true;
		$imageType     = getimagesize($tweetImg);
	}
	else {
		$uploadedImage = false;
	}


	// Posta en tweet
	if ($tweet != "" && mb_strlen($tweet,"UTF-8") <= 150) {

		// om det finns en bild med - lägg in bild och tweet
		if ($uploadedImage) {
			
			if (isValidImage($tweetImg,$imgError,$imgSize)) {
				$tweet     = mysqli_real_escape_string($link,$tweet);
				mysqli_query($link, $insertTweetQuery);
			    $tweet_id  = mysqli_insert_id($link);
				$imageData = addslashes(file_get_contents($_FILES['tweetImage']['tmp_name']));
				mysqli_query($link, "UPDATE tweets SET `image` = '$imageData' WHERE id = $tweet_id");
				mysqli_error($link);
			}
			else {
				$imageError = "Not a valid image";				
			}
		}

		// Om det bara finns en tweet - posta den.
		else {

			$tweet = mysqli_real_escape_string($link,$tweet);			
			mysqli_query($link, $insertTweetQuery);
			$tweet_id  = mysqli_insert_id($link);

		}

	}
	
	else {
		$tweetError = "Not a valid Tweet";
	}

	notify($link,$username, $tweet_id,$tweet);
	addHashtag($link,$username,$tweet_id,$tweet);

};






// Hämtar sökvärden

// if (!empty($_POST['message']) && isset($_POST['submitAnswer'])) {
// 	addMessageToDb($link);
// }



// Hämtar konversationsmeddelanden med specifik Tweet-id // 
if (isset($_GET['id'])) {
	$tweet_id = (int) $_GET['id'];

	$conversationResult   = mysqli_query($link, 
		"SELECT DISTINCT 
		conversations.username AS user, 
		tweets.id, 
		conversations.id,
		conversations.message,
		tweets.tweet

		FROM conversations 
		INNER JOIN tweets ON tweets.id = conversations.tweet_id 
		WHERE tweet_id = $tweet_id"); 

	
// hämtar själva tweeten som är vald
	$selectedTweetResult  = mysqli_query($link, 
		"SELECT tweet 
		 FROM tweets 
		 WHERE id = $tweet_id"); 
	
};

// lägger in konversationsmeddelanden i databas



// Visar alla tweets som finns på förstasidan
$frontPageTweetQuery   = mysqli_query($link,
	"SELECT 
	tweets.tweet,
	tweets.user_id,
	tweets.id AS tweetId,
	user.username, user.id,
	tweets.image AS tweetImage, 
	tweets.date AS date
	FROM user 
	INNER JOIN tweets ON user.id = tweets.user_id 
	ORDER BY tweets.id DESC"); 


$frontPageTweetQuery2 = mysqli_query($link, 
	"SELECT DISTINCT
	tweets.tweet,
	tweets.user_id,
	tweets.id AS tweetId,
	tweets.username AS username,
	tweets.image AS tweetImage,
	tweets.date AS date
	FROM followers INNER JOIN tweets ON followers.following = tweets.username OR tweets.username = '{$_SESSION['username']}'
	WHERE followers.user_id = $user_id ORDER BY tweets.id DESC"); 



// Ändra profilinställningar // 

if (isset($_POST['submitProfileImage'])) {

	if(!empty($_POST['changeDesc'])) {
		$description = $_POST['changeDesc']; 
		if (strlen($description) < 50) {
			$description = mysqli_real_escape_string($link,$description);
			mysqli_query($link, "UPDATE user SET `description` = '$description' WHERE `id` = " . $user_id . " " );
		}
	}

	if (file_exists($_FILES['image']['tmp_name']) && !empty($_FILES['image']['tmp_name'])) {
		$filename = $_FILES['image']['name']; 
		$size = $_FILES['image']['size'];
		$imgData = addslashes(file_get_contents($_FILES['image']['tmp_name']));
		
		$updateProfileImage = "UPDATE user SET `image`= '$imgData' WHERE `id` = " . $user_id . " " ;
		
		mysqli_query($link,$updateProfileImage);
		echo mysqli_error($link);
	}

	if(isset($_POST['changeName']) && $_POST['changeName'] != "") {
		$name = $_POST['changeName']; 
		if (strlen($name) < 50) {
			$user = $_SESSION['username'];
			$description = mysqli_real_escape_string($link,$description);
			mysqli_query($link, "UPDATE user SET `firstname` = '$name' WHERE `id` = " . $user_id . " " );
		}

	}



}

// hämtar vilket user_id som används
if (isset($_GET['user'])) {
	$user = mysqli_real_escape_string($link,$_GET['user']);
	$userIdQuery = mysqli_query($link, "SELECT id, firstname, lastname, email FROM user WHERE username = '$user'");
	
	$userrow = mysqli_fetch_assoc($userIdQuery);
		 $user_id = $userrow['id'];
		 $uname = $userrow['id'];
}
	


// tar bort tweet 
	if (isset($_POST['deleteSubmit'])) {
		$tweet_id = $_POST['delete'];
		mysqli_query($link,"DELETE FROM tweets WHERE `id` = $tweet_id");
		mysqli_error($link);
	}

// hämtar profilbeskrivningen 
if (isset($_GET['user'])) {
	$user = mysqli_real_escape_string($link,$_GET['user']);
	$userDescriptionQuery = mysqli_query($link, "SELECT description FROM user WHERE username = '$user'");
	$userDescription = mysqli_fetch_assoc($userDescriptionQuery);
}
else {
		$user = $username;
		$userDescriptionQuery = mysqli_query($link, "SELECT description FROM user WHERE username = '$user'");
		$userDescription = mysqli_fetch_assoc($userDescriptionQuery);
}

// hämtar trending hashtags

mysqli_query($link, "SELECT tagname FROM ")




?>