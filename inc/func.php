<?php

// ****** FORMATERAR TWEET MED HASHTAGS, TAGGAR, LÄNKAR ********* / 
function hashtagsAndUser($str)
{
    
    // Hittar ord som är hashtag och gör de till länkar
    $regex = "/(#)+[a-zA-Z0-9]+/";
    
    if (preg_match($regex, $str, $match2)) {
    	preg_match($regex, $str, $match2);
    	$hash = trim(@$match2[0], "#");
    	$str   = preg_replace($regex, "<a href='hashtag.php?hash=" . $hash . "')'>\\0</a>", $str);
	}
    
    // hittar användare med @-tagg och gör de till länkar
    $regex2 = "/(@)+[a-zA-Z0-9]+/";
    preg_match($regex2, $str, $match);
    $user = trim(@$match[0], "@");
    
    
    if ($user == $_SESSION['username']) {
        $str = preg_replace($regex2, "<a href='profile.php')'>\\0</a>", $str);
    } else {
        $str = preg_replace($regex2, "<a href='index.php?user=$user')'>\\0</a>", $str);
    }
    
    $str = makeLink($str);
    return ($str);
}

function makeLink($str)
{
    
    $reg_exUrl = "/(http|https|ftp|ftps)\:\/\/[a-zA-Z0-9\-\.]+\.[a-zA-Z]{2,3}(\/\S*)?/";
    $reg_www   = "/(www\.)+[a-zA-Z0-9.\-\.]+[a-zA-Z]+/";
    
    
    if (preg_match($reg_exUrl, $str, $url)) {
        
        $tinyUrl = getTinyurl($url[0]);
        return preg_replace($reg_exUrl, "<a href=" . $url[0] . ">" . $tinyUrl . "</a> ", $str);
        
    } else if (preg_match($reg_www, $str, $url)) {
        
        $tinyUrl = getTinyurl($url[0]);
        return preg_replace($reg_www, "<a href=" . $url[0] . ">" . $tinyUrl . "</a> ", $str);
    } else {
        return $str;
    }
}


// ******* NOTIFICATIONS *********  // 
function displayNotificationNumber($username, $link)
{
    $notiQuery  = mysqli_query($link, "SELECT * FROM notification WHERE `seen` = 1 AND to_user = '{$_SESSION['username']}'");
    $notiNumber = mysqli_num_rows($notiQuery);
    
    if ($notiNumber > 0) {
        $notiNumber = $notiNumber;
    } else {
        return $notiNumber;
    }
    
    return $notiNumber;
    
}

function displayNotifications($link)
{
    
    $seen  = displayNotificationNumber($_SESSION['username'], $link);
    $query = mysqli_query($link, "SELECT tweets.id AS id, tweets.tweet AS tweet, tweets.image AS image, tweets.username AS username, notification.seen AS seen, tweets.date AS date FROM notification INNER JOIN tweets ON tweets.id = notification.tweet_id WHERE `seen` = 1");
    // skriver ut de osedda taggar
    while ($row = mysqli_fetch_assoc($query)) {
        
        $tweet = $row['tweet'];
        $id    = $row['id'];
        $date  = $row['date'];
        $image = $row['image'];
        $username = $row['username'];
        
        if ($seen != 0) {
            printTweet($username, $tweet, $id, $link, $image, $date);
            mysqli_query($link, "UPDATE `notification` SET `seen` = '0' WHERE to_user = '{$_SESSION['username']}'");
            
        }
    }
    
    if ($seen == 0) {
        print "<div class='nonoti'>";
        print "<h2 class='noti'> No new notifications </h1>";
        print "<i class='ion-sad-outline'></i>";
        print "</div>";
    }
}




// ********** SKRIVER UT TWEET ******** // 
function printTweet($username, $tweet, $tweetId, $link, $image, $date)
{
    
    print "<div id='tweet" . "$tweetId' class='tweet'>";
    
    if ($username == $_SESSION['username']) {
        print "<i id='$tweetId' class='ion-close deleteTweet'></i>";
    }
    
    print "<div class='miniPicture'>";
    displayProfilePicture($link, $username);
    print "</div>";
    
    print "<div class='tweetText'>";
    print createLinks($username);
    print "<span style='font-size: 10px; margin-left:10px'>" . $date . "</span>";
    
    print "<p>";
    print hashtagsAndUser(strip_tags($tweet, "<a></a>"));
    print "</p>";
    print "</div>";
    
    if ($image != "") {
        $image = base64_encode($image);
        print '<div id="tweetImage' . $tweetId . '" class="imageclosed tweetImage"><img src="data:image/jpeg;base64,' . $image . '" /></div>';
    }
    print displayMessageNumber($link, $tweetId);
    print "</div>";
    
    print "<div id='undertweet" . $tweetId . "' class='underTweet'>";
    underTweetLinks($username, $tweetId, $link);
    
    print "<div class='conversation" . $tweetId . "'>";
    underTweetMessages($link, $tweetId);
    print "</div>";
    print "</div>";
    
}

function displayMessageNumber($link, $tweetId) {
    $conversationResult = mysqli_query($link, "SELECT DISTINCT 
        conversations.username AS user, 
        tweets.id, 
        conversations.id,
        conversations.message,
        tweets.tweet

        FROM conversations 
        INNER JOIN tweets ON tweets.id = conversations.tweet_id 
        WHERE tweet_id = $tweetId");

    $number = mysqli_num_rows($conversationResult);
    return "<span class='convoNumber'><i class='ion-ios-chatbubble-outline'></i>" . $number . "</span>";

}

// ******  VISAR FÖRSTASIDANS TWEETS  ********* // 

function displayFrontpageTweets($link, $query)
{
    
    $num = mysqli_num_rows($query);
    while ($row = mysqli_fetch_assoc($query)) {
        
        $tweet      = $row['tweet'];
        $username   = $row["username"];
        $tweetId    = $row['tweetId'];
        $tweetImage = $row['tweetImage'];
        $tweetDate  = $row['date'];
        
        printTweet($username, $tweet, $tweetId, $link, $tweetImage, $tweetDate);
    }
    if ($num == 0 ) {
        print "<div class='nonoti'>";
        print "<h2 class='noti'> Go ahead and <br> follow people! </h1>";
        print "<i class='ion-happy-outline'></i>";
        print "<p> tips: search for elinaaberg </p>";
        print "</div>";
    }
    
}

// ******** VISAR TWEET FRÅN EN SPECIFIK ANVÄNDARE  ******** // 

function displayUserTweets($username, $link, $user_id)
{
    
    $userResultQuery = mysqli_query($link, "SELECT 
	tweets.image as tweetImage,
	tweets.user_id AS 
	user_id, tweets.id AS tweetId,
	tweets.tweet AS tweet,
	user.id AS id, 
	tweets.date AS date
	FROM tweets 
	INNER JOIN user ON user.id = tweets.user_id WHERE tweets.user_id = $user_id 
	ORDER BY tweets.id DESC");
    
    $count = 0;
    $num = mysqli_num_rows($userResultQuery);
    
    while ($row = mysqli_fetch_assoc($userResultQuery)) {
        $user_id    = $row["id"];
        $tweetId    = $row['tweetId'];
        $tweet      = $row['tweet'];
        $tweetImage = $row['tweetImage'];
        $tweetDate  = $row['date'];
        
        if ($count == 0) {
            $count++;
        }
        
        print mysqli_error($link);
        
        printTweet($username, $tweet, $tweetId, $link, $tweetImage, $tweetDate);
        
    }
     
     if ($num == 0 ) {
        print "<div class='nonoti'>";
        print "<h2 class='noti'> No LOLs to show</h1>";
        print "<i class='ion-sad-outline'></i>";
        print "</div>";
    }
}




// ******** LÄNK TILL SPECIFIK ANVÄNDARE ******** // 
function createLinks($str)
{
    if ($str == $_SESSION['username']) {
        return "<a href='profile.php'>" . $str . " </a> ";
    } else {
        return "<a href='index.php?user=" . $str . "'>" . $str . " </a> ";
    }
}

// ******** LÄNKAR UNDER TWEETEN ******** // 
function underTweetLinks($str, $tweetId, $link)
{
    print "<div id='message" . $tweetId . "'>";
    print "<input type='hidden' class='messageTweetId' value=$tweetId>";
    print "<input type='text' placeholder='say something nice'  id='messageTweet" . $tweetId . "' name='message'>";
    print "<button class='submitAnswer' id='submitAnswer-" . $tweetId . "' name='submitAnswer'>Answer</button>";
    print "</div>";
}




// ******* VISAR SÖKRESULTAT ******** // 


function displayUserSearch($result, $search)
{
    
    while ($row4 = mysqli_fetch_assoc($result)) {
        $us = $row4['username'];
        
        print createlinks($us);
    }
    
}


// ****** KOLLAR SÅ ATT MAN ÄR INLOGGAD ******* // 

function checkLoggedIn()
{
    if (empty($_SESSION['loggedin'])) {
        header("Location: login.php");
    }
    
    if (isset($_SESSION["loggedin"])) {
        print "WELCOME TO TWITTER 2.0";
    }
}

function displayProfilePicture($link, $user)
{
    
    $query = mysqli_query($link, "SELECT image FROM user WHERE username = '$user'");
    $row   = mysqli_fetch_assoc($query);
    $image = base64_encode($row['image']);
    print "<a href='index.php?user=" . $user . "'>";
    print '<img src="data:image/jpeg;base64,' . $image . '" /></a>';
    
}
;


// SKAPAR TINYURL // 

function getTinyUrl($url)
{
    $ch      = curl_init();
    $timeout = 5;
    
    curl_setopt($ch, CURLOPT_URL, 'http://tinyurl.com/api-create.php?url=' . $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
    
    $data = curl_exec($ch);
    curl_close($ch);
    return $data;
}



function isValidImage($image, $error, $size)
{
    $valid     = true;
    $imageType = getimagesize($image);
    if (!is_file($image)) {
        $valid = false;
    }
    if ($error != 0) {
        $valid = false;
    }
    if ($size > 1000000) {
        $valid = false;
    }
    
    if ($imageType[2] == 0) {
        $valid = false;
    }
    
    return $valid;
}


function underTweetMessages($link, $tweetId)
{
    
    $conversationResult = mysqli_query($link, "SELECT DISTINCT 
		conversations.username AS user, 
		tweets.id, 
		conversations.id,
		conversations.message,
		tweets.tweet

		FROM conversations 
		INNER JOIN tweets ON tweets.id = conversations.tweet_id 
		WHERE tweet_id = $tweetId");
    
    while ($row1 = mysqli_fetch_assoc($conversationResult)) {
        $messages = filter_var($row1['message'], FILTER_SANITIZE_STRING);
        $username = $row1['user'];
        
        
        print createLinks($username);
        print hashtagsAndUser($messages);
        print "<br>";
    }
}

// hämtar antal followers

function getFollowersNum($link, $username)
{
    $result = mysqli_query($link, "SELECT count(following) AS count FROM `followers` WHERE following = '$username'");
    $number = mysqli_fetch_array($result);
    return $number['count'];
}

function getFollowingNum($link, $user_id) {
    $result = mysqli_query($link, "SELECT count(following) AS count FROM `followers` WHERE user_id = $user_id");
    $number = mysqli_fetch_array($result);
    return $number['count'];
}

function getFollowing($link, $user_id) {
    $result = mysqli_query($link, "SELECT following, image, username FROM `followers` INNER JOIN `user` ON followers.following = user.username WHERE user_id = '$user_id'");
    while ($row = mysqli_fetch_assoc($result)) {
         $u =  $row["following"];
         $image =  $row["image"];
         $image = base64_encode($row['image']);
        print "<a href='index.php?user=" . $u . "'>";
        print '<img width="30px" height="30px" src="data:image/jpeg;base64,' . $image . '" /></a>';

    }
}

function getFollowers($link, $username) {
    $result = mysqli_query($link, "SELECT username, image FROM `followers` INNER JOIN `user`ON followers.user_id = user.id WHERE following = '$username'");
    while ($row = mysqli_fetch_assoc($result)) {
         $u =  $row["username"];
         $image =  $row["image"];
         $image = base64_encode($row['image']);
        print "<a href='index.php?user=" . $u . "'>";
        print '<img width="30px" height="30px" src="data:image/jpeg;base64,' . $image . '" /></a>';

    }
}



function checkIfFollowing($link, $username, $user_id)
{
    $result = mysqli_query($link, "SELECT * FROM `followers` WHERE user_id = $user_id AND following = '$username'");
    $count  = mysqli_fetch_row($result);
    
    if ($count[0] == 0) {
        return "<h3 id='f' class='follow'>follow</h3>";
    } else {
        return "<h3 id='f' class='unfollow'>unfollow</h3>";
    }
}


// skapar notis om någon taggas i ett inlägg / kommentar
function notify($link, $username, $tweet_id, $tweet)
{
    $regex2 = "/(@)+[a-zA-Z0-9]+/";
    preg_match($regex2, $tweet, $match);
    $user = trim(@$match[0], "@");
    
    if ($user != "") {
        mysqli_query($link, "INSERT INTO notification (`from_user`, `to_user`, `notification`, `seen`, `tweet_id`) 
			 VALUES ('$username', '$user', 'new tag', 1, $tweet_id)");
    }
    ;
}

function addHashtag($link, $username, $tweet_id, $tweet)
{
    
    $regex2 = "/(#)+[a-zA-Z0-9]+/";
    
    if (preg_match($regex2, $tweet, $match)) {
        
        preg_match($regex2, $tweet, $match);
        $hash = mysqli_real_escape_string($link,$match[0]);
        
        mysqli_query($link, "INSERT INTO hashtags (`tagname`, `tweet_id`) 
			 VALUES ('$hash', $tweet_id)");
    }
    ;
}

function trendingHash($link)
{
    $query = mysqli_query($link, "SELECT tagname, COUNT(tagname) 
  		FROM hashtags 
  		GROUP BY tagname LIMIT 5");
    
    while ($row = mysqli_fetch_array($query)) {
        $hash = $row['tagname'];
        print "<span class='hashtags'>" . hashtagsAndUser($hash) . "</span>";
    }
}

function displayHashtag($link, $hashtag){

	$hashtag = "#" . $hashtag;
   
    $query = mysqli_query($link, "SELECT tweets.tweet, hashtags.tagname, tweets.id, tweets.date, tweets.image, tweets.username FROM tweets INNER JOIN hashtags ON hashtags.tweet_id = tweets.id AND hashtags.tagname = '$hashtag'");
   	$num = mysqli_num_rows($query);
    // skriver ut de osedda taggar
    while ($row = mysqli_fetch_assoc($query)) {
        
        $tweet    = $row['tweet'];
        $id       = $row['id'];
        $date     = $row['date'];
        $image    = $row['image'];
        $username = $row['username'];
        
        printTweet($username, $tweet, $id, $link, $image, $date);
    }

    if ($num == 0) {
    	print "<div class='nonoti'>";
        print "<h2 class='noti'> No hashtags </h1>";
        print "<i class='ion-sad-outline'></i>";
        print "</div>";
    }
}



?>