$( document ).ready(function() {

	// ANIMERAR FLIKARNA
    
	$(".left-flik .icon").click(function(){
		var flik = $(this).closest(".left-flik");
		if ($(flik).hasClass("isOpen")) {
			$("#left-sidebar").css("z-index","0");
			$(flik).animate({left: "230"},"fast");
			$(flik).removeClass("isOpen");
		}
		else {
			$(flik).animate({left:"0"},"fast");
			$("#left-sidebar").css("z-index","1");
			$(flik).addClass("isOpen");
		};
	return false;
	});


	$(".right-flik .icon").click(function(){
		var flik2 = $(this).closest(".right-flik");

		if ($(flik2).hasClass("isOpen")) {
			$(".sidebar").css("z-index",0);
			$(flik2).animate({right: "230"},"fast");
			$(flik2).removeClass("isOpen");
		}
		else {
			$(flik2).animate({right:"0"},"fast");
			$(".sidebar").css("z-index","1");
			$(flik2).addClass("isOpen");
		};
		return false;
	});

	// ANIMERAR TWEETSEN

	$(".tweet").on("click",function(){
		var target = $(this);
		if (target.is("a")) {
			return
		}
		if (target.is("img")) {
			return
		}
		else if(target.is("i")) {
			return
		}
	else {
		
		$(this).toggleClass('activeTweet');
		$(this).next(".underTweet").slideToggle();
	}

	})

	//ANIMERAR NOTIFICATIONSFLIKEN

	if ($(".noti").attr("id") == 0) {
		$(".noti").addClass("notNo");
		$(".noti").removeClass("animated");
		$(".noti").removeClass("bounce");
	}
	else {
		$(this).addClass("notYes");
	}


	// 
	$('.signup').on("click",function() {
		$("#member-box").removeClass("hidden");
	})

	
	// VALIDERING SIGN UP // 

	$("input.sign").keyup(function() {
		var value = $(this).val();
		var input = $(this);

		 if (value.length > 20 ) {
		   		$(input).css("background-color","pink");
		   	}
		   	else {
		   		$(input).css("background-color","white");
		   	}
	   



	})

	$("#submitsignup").click(function(event) {


		
	});


	$(".infotext h2").click(function(){
		$(".infotable").slideToggle('hidden');
	})

	$(document).on("click",".signup",function(event){
		
		$('section#member-box').removeClass("hidden");
		var target = "#" + this.getAttribute('data-target');
		
		$('html,body').animate({
			scrollTop: $(target).offset().top},500);
		
	});	

	// FOLLOWINGKNAPPEN 

	$('.followerbox').on("click",function() {
		
		var data = $(this).attr("id");
		var user =  data.split("_").pop();

		if ($("h3#f").hasClass('follow')) {
			
			$("h3#f").addClass("unfollow");
			$("h3#f").removeClass("follow");
			$("h3#f").text("unfollow");

			$.ajax({
				url: "inc/follow.php",
				type: "get",
				data: "follow=" + user,
				dataType: "html",
				success: function(data) {
					console.log("good");
				}
			});

		}
		else  {

			$.ajax({
				url: "inc/follow.php",
				type: "get",
				data: "unfollow=" + user,
				dataType: "html",
				success: function(data) {
					console.log("good");
				
				}
			});

			$("h3#f").removeClass("unfollow");
			$("h3#f").addClass("follow");
			$("h3#f").text("follow");
			

			
		}
	})

	$("span.folnum").on("click", function () {
		$("tr.hiddenrow").slideToggle();
	})

	$("span.folnum2").on("click", function () {
		$("tr.hiddenrow2").slideToggle();
	})


// AJAX FÖR SÖKFUNKTION
	$('#searchform input').on("keyup",function() {
		var data = $(this).val();
		$.ajax({
			data: "search=" + data,
			url: "inc/follow.php",
			type: "get",
			dataType: "html",
			success: function(data) {
				$("#searchResult").html(data + "<br>");
				
			}

		})

	})



// TA BORT TWEET
	$('.deleteTweet').on("click", function() {

		var id = $(this).attr("id");
		var tweetid = ("#tweet" + id);

		var tweet      = ("#tweet" + id);
		var underTweet = ("#undertweet" + id);

		$(underTweet).remove();
		$(tweet).remove();

		var button = $(this).attr("id");
		$.ajax({
			data: "delete=" + button,
			url: "inc/follow.php",
			type: "POST",
			dataType: "html",
			success: function(data) {
				$(tweetid).hide("slow",function() {
					
				});
			}
		});
	});

	// LÄGGA TILL RETWEET-MEDDELANDE I DATABAS 
	$('button.submitAnswer').on("click", function() {
		var d = $(this).attr("id").split("-");
		var tweetId = d[1];
		var message = $("input#messageTweet" + tweetId).val();
		var conversation = ".conversation" + tweetId;


		$.ajax({
			type: "POST",
			data: {message : message, tweetId : tweetId},
			url: "inc/follow.php",
			dataType: "html",
			success: function(data) {
				if (message.length > 0) {

				var obj = $.parseJSON(data);
				var s = obj.message;
				
				var str = obj.username + " " + s + "<br>";
				$(conversation).append(str);
				};

			}
		})
	})

	// gör så att storleken på textrutan ändras beroende på bild 
	$('.tweet').each(function(){

		if ($(this).find(".tweetImage").length > 0) { 
		var id = $(this).attr("id");
		console.log(id);
		$("#" + id + " .tweetText").css("width","50%");
		}
		else {
			$(id).find(".tweetImage").addClass("no");
		}


	})
	
	// LIGHTBOX 
	$('.tweetImage').on("click",function() {
						
		var img = $(this).find("img").attr('src');
		$(".bigbox").fadeIn();
		$('.lightbox').fadeIn(); 
		$('.lightbox').html("<img src='"+img+"' alt='light'>");
	})

	
	$(".lightbox").on("click", function() {
		$(this).fadeOut();
		$(".bigbox").fadeOut()	;
	})

	
	// MOBILMENY

	$(".iconmobile").on("click", function() {
		var icon = $(this);
		$(icon).siblings().removeClass("active");
		var target = $(this).data("target"); 
			
		if ($(this).hasClass("active")) {
			$('.mobile').animate({height: "0px"})
			$(this).removeClass("active");
			$('.mobile #' + target).hide().siblings().hide();

		}
		else {
			$(icon).addClass("active");
			var target = $(this).data("target"); 
			$('.mobile').animate({height: "300px"});
			$('.mobile #' + target).fadeIn().siblings().hide();
		}


	})


		$("input#searchform2").on("keydown", function() {
			var data = $(this).val();
			$.ajax({
				data: "search2=" + data,
				url: "inc/follow.php",
				type: "get",
				dataType: "html",
				success: function(data) {
					$("#searchResult2").html(data);
				}
			})
		})



});


		
	// COUNTER FÖR ANTAL TECKEN I TWEETS 

	function counter() {
			var message = document.getElementById('lol');
			var div = document.getElementById('counter');
			var len = message.value.length;
			div.innerHTML = 150 - len; 
			
			if (len > 150) {
				div.style.color = "red";
			}
			else {
				div.style.color = "black";
			}
		}