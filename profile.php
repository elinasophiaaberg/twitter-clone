<?php include_once("header.php") ?> 

	<!-- <h1 class="logo"> Elina Åberg </h1> -->
	<?php $user_id = $_SESSION['id']; ?> 

	<!-- PROFILBILD  -->
	<div id="profileheader">
		<h2 class="profiletext"><?php print htmlspecialchars($userDescription['description']); ?></h2>
		<div class="profile-picture">
			<?php displayProfilePicture($link, $user); ?>
		</div>
	</div>
	<div class="infotext">
		<h2> Your profile </h2> 
	
	<!-- GÖMD PROFILBESKRIVNING -->
	<div class="infotable hidden">
		<table>
			<tr>
				<td>Username</td><td><?php print $username;?></td>
			</tr>
			<tr>
				<td>Firstname</td> <td><?php print $firstname;?></td>
			</tr>
			<tr>
				<td>Lastname</td><td><?php print $lastname;?></td>
			</tr>
			<tr>
				<td>Email</td><td><?php print $email;?></td>	
			</tr>
			<tr>
				<td>Followers</td><td><?php print "<span class='folnum2'>" . getFollowersNum($link,$user) . "</span>";?></td>
			</tr>
			<tr class="hiddenrow2"><td></td><td> <?php print getFollowers($link,$username);?> </td></tr>
			<tr>
				<td>Following</td><td><?php print "<span class='folnum'>" . getFollowingNum($link,$user_id) . "</span>";?> 									 
				</td>
			</tr>
			<tr class="hiddenrow"><td></td><td> <?php print getFollowing($link,$user_id);?> </td></tr>
		</table>
	</div>
	 </div>
	<!-- VISAR ANVÄNDARENS TWEETS -->
	<div id="tweetContent">
		<?php displayUserTweets($_SESSION['username'],$link, $_SESSION['id']); ?> 
	</div>

</div>


	<script src="js/script.js"> </script>


</body>
</html>

