<?php

$user = $_GET['user'];
$user_id = $userrow['id'];
$firstname = $userrow['firstname'];
$lastname = $userrow['lastname'];
$email = $userrow['email'];

?>

		<div id="profileheader">
		<h2 class="profiletext"><?php print htmlspecialchars($userDescription['description']); ?>  </h2>
		<div class="profile-picture">
			<?php displayProfilePicture($link, $user); ?>
		</div>
	</div>
		<div class="infotext">
			<h2> <?php print $user;?>'s profile </h2> 
			
			<div class="infotable hidden">
		<table>
			<tr>
				<td>Username</td><td><?php print $user;?></td>
			</tr>
			<tr>
				<td>Firstname</td> <td><?php print $firstname;?></td>
			</tr>
			<tr>
				<td>Lastname</td><td><?php print $lastname;?></td>
			</tr>
			<tr>
				<td>Email</td><td><?php print $email;?></td>	
			</tr>
			<tr>
				<td>Followers</td><td><?php print "<span class='folnum2'>" . getFollowersNum($link,$user) . "</span>";?></td>
			</tr>
			<tr class="hiddenrow2"><td></td><td> <?php print getFollowers($link,$user);?> </td></tr>

			<tr>
				<td>Following</td><td><?php print "<span class='folnum'>" . getFollowingNum($link,$user_id) . "</span>";?> 									 
				</td>
			</tr>
			<tr class="hiddenrow"><td></td><td> <?php print getFollowing($link,$user_id);?> </td></tr>
		</table>
	</div>
	<div id="<?php print 'follow_' . $user ; ?>" class="followerbox">
				
				<?php print checkIfFollowing($link,$user,$_SESSION['id']); ?> 

			</div>
		</div>
			

		<div id="tweetContent">
		<?php displayUserTweets($_GET['user'],$link, $user_id); ?>
